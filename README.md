# devops-mock-service

## Overview

- The mock service is a simple Node+Express app with a single /status endpoint.
- It terminates at launch, roughly around 1 in 5 times.
- It contains a `default/config.json` file where you can configure the `service_cycle_duration` parameter (set in seconds).
- Once started, it keeps running and providing a valid 200 status for 2/3rds of the `service_cycle_duration`.
- For the remaining 1/3rd duration of `service_cycle_duration`, it returns a 500 status.
- After this duration of time, the app terminates with a non-zero exit status.

## Installation

You will need to install a reasonably recent version of Node, preferably 8.10.0 LTS for running the mock service. 

- You can use nvm to ease this.

`npm install && npm start` should get the services running on port 19090.
