var express = require('express');
var router = express.Router();

const appState = {
  RESPONSIVE: 0,
  NON_RESPONSIVE: 1,
};

function createMockStatusHandler(runCycleDuration) {
  let currentState = appState.RESPONSIVE;
  setTimeout(function() {
    console.log('Moving to non-responsive status');
    currentState += 1;
  }, 1000 * runCycleDuration * 2.0 / 3.0);

  return function(req, res) {
    if (currentState === appState.RESPONSIVE) {
      res.send('OK');
      return;
    }
    res.status(500).send('ERROR');
  };
}

module.exports = createMockStatusHandler;
